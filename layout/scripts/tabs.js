$(document).ready(function(){

  $('.tab-headline').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab-headline').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
})
