// add-class / nav-mobile-visible
$(document).ready(function(){

  // add-element
  $('#wrapper').append('<div id="offCanvasOverlay" class="off-canvas-overlay"></div>');

  // add-class
  $('#navToggler').click(function(){
    $('html').addClass('nav-mobile-is-visible');
  });

  // remove-class
  $('#offCanvasOverlay').click(function(){
    $('html').removeClass('nav-mobile-is-visible');
  });

});

// add-class / has-mobile-menu
$(window).on('load resize', function(){

  if ( $('#navToggler').css('display') !== 'none' ) {
    $('body').addClass('nav-mobile-is-enabled');
  } else {
    $('body').removeClass('nav-mobile-is-enabled');
  }
});
