var $arr = [
  "fadeIn",
  "fadeInDown",
  "fadeInDownBig",
  "fadeInLeft",
  "fadeInLeftBig",
  "fadeInRight",
  "fadeInRightBig",
  "fadeInUp",
  "fadeInUpBig",
  "zoomIn"
];
jQuery.each( $arr, function(i, val) {
  $(".animation-" + val).addClass('animated');
  $(".animation-" + val).waypoint(function(direction) {
    if (direction === 'down') {
      $(this.element).addClass(val);
    } else if (direction === 'up') {
      $(this.element).removeClass(val);
    }
  }, {offset: '100%'});
});
