$(document).ready(function(){
  $('.slider').slick({
    prevArrow: '<div class="slick-prev button"><button></button></div>',
    nextArrow: '<div class="slick-next button"><button></button></div>',
    dots: true
  });
});
